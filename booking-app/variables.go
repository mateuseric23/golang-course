package main

import "fmt"

func main() {

	// fmt.Println("Welcome to our conference booking application")
	// fmt.Println("Get your tickets here to attend")

	// var conferenceName = "Go Conference"
	// _ = conferenceName
	// fmt.Println(conferenceName)

	var conferenceName = "Go Conference"
	const conferenceTickets = 50
	var remainingTickets = 50
	_ = remainingTickets

	fmt.Println("Welcome to", conferenceName, "booking application")
	fmt.Println("We have total of", conferenceTickets, "tickets and", remainingTickets, "are still available.")
	fmt.Println("Get your tickets here to attend")

}
