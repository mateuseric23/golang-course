package main

import "fmt"

func main() {

	// fmt.Println("Welcome to our conference booking application")
	// fmt.Println("Get your tickets here to attend")

	// var conferenceName = "Go Conference"
	// _ = conferenceName
	// fmt.Println(conferenceName)

	var conferenceName = "Go Conference"
	const conferenceTickets = 50
	var remainingTickets = 50
	_ = remainingTickets

	fmt.Printf("Welcome to %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are still available.\n", conferenceTickets, remainingTickets)
	fmt.Println("Get your tickets here to attend")

}
