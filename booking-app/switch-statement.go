package main

import (
	"fmt"
	"strings"
)

func main() {

	// fmt.Println("Welcome to our conference booking application")
	// fmt.Println("Get your tickets here to attend")

	// var conferenceName = "Go Conference"
	// _ = conferenceName
	// fmt.Println(conferenceName)

	conferenceName := "Go Conference"
	const conferenceTickets = 50
	var remainingTickets uint = 50
	bookings := []string{}

	fmt.Printf("Conference Tickets is %T, Remaining Tickets is %T, Conference Name is %T\n", conferenceTickets)

	fmt.Printf("Welcome to %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are still available.\n", conferenceTickets, remainingTickets)
	fmt.Println("Get your tickets here to attend")

	// for {

	// 	var firstName string
	// 	var lastName string
	// 	var email string
	// 	var userTickets uint
	// 	//fmt.Scan(&userName)

	// 	// fmt.Println(remainingTickets)
	// 	// fmt.Println(&remainingTickets)

	// 	//ask user to their name

	// 	// userName = "Tom"
	// 	// userTickets = 2

	// 	// fmt.Printf("User %v booked %v tickets.\n", userName, userTickets)

	// 	fmt.Println("Enter your first name: ")
	// 	fmt.Scan(&firstName)

	// 	fmt.Println("Enter your last name: ")
	// 	fmt.Scan(&lastName)

	// 	fmt.Println("Enter your email address: ")
	// 	fmt.Scan(&email)

	// 	fmt.Println("Enter number of tickets: ")
	// 	fmt.Scan(&userTickets)

	// 	if userTickets <= remainingTickets {
	// 		remainingTickets = remainingTickets - userTickets
	// 		bookings = append(bookings, firstName+" "+lastName)

	// 		// fmt.Printf("The whole array: %v\n", bookings)
	// 		// fmt.Printf("The first value: %v\n", bookings[0])
	// 		// fmt.Printf("Slice type: %T\n", bookings)
	// 		// fmt.Printf("Slice length: %v\n", len(bookings))

	// 		fmt.Printf("Thank you %v %v for booking %v tickets. You will receive a confirmation email at %v\n", firstName, lastName, userTickets, email)
	// 		fmt.Printf("%v tickets remaining for %v\n", remainingTickets, conferenceName)

	// 		firstNames := []string{}
	// 		for _, booking := range bookings {
	// 			var names = strings.Fields(booking)
	// 			firstNames = append(firstNames, names[0])
	// 		}

	// 		fmt.Printf("The first names of bookings are: %v\n", firstNames)

	// 		if remainingTickets == 0 {
	// 			// end program
	// 			fmt.Println("Our Conference is booked out. Come back next year.")
	// 			break
	// 		}

	// 	} else {
	// 		fmt.Printf("We only have %v tickets remaining, so you can't book %v tickets\n", remainingTickets, userTickets)
	// 	}

	// }

	for remainingTickets > 0 && len(bookings) < 50 {

		var firstName string
		var lastName string
		var email string
		var userTickets uint
		//fmt.Scan(&userName)

		// fmt.Println(remainingTickets)
		// fmt.Println(&remainingTickets)

		//ask user to their name

		// userName = "Tom"
		// userTickets = 2

		// fmt.Printf("User %v booked %v tickets.\n", userName, userTickets)

		fmt.Println("Enter your first name: ")
		fmt.Scan(&firstName)

		fmt.Println("Enter your last name: ")
		fmt.Scan(&lastName)

		fmt.Println("Enter your email address: ")
		fmt.Scan(&email)

		fmt.Println("Enter number of tickets: ")
		fmt.Scan(&userTickets)

		isValidName := len(firstName) >= 2 && len(lastName) >= 2
		isValidEmail := strings.Contains(email, "@")
		isValidTicketNumber := userTickets > 0 && userTickets <= remainingTickets

		if isValidName && isValidEmail && isValidTicketNumber {
			remainingTickets = remainingTickets - userTickets
			bookings = append(bookings, firstName+" "+lastName)

			// fmt.Printf("The whole array: %v\n", bookings)
			// fmt.Printf("The first value: %v\n", bookings[0])
			// fmt.Printf("Slice type: %T\n", bookings)
			// fmt.Printf("Slice length: %v\n", len(bookings))

			fmt.Printf("Thank you %v %v for booking %v tickets. You will receive a confirmation email at %v\n", firstName, lastName, userTickets, email)
			fmt.Printf("%v tickets remaining for %v\n", remainingTickets, conferenceName)

			firstNames := []string{}
			for _, booking := range bookings {
				var names = strings.Fields(booking)
				firstNames = append(firstNames, names[0])
			}

			fmt.Printf("The first names of bookings are: %v\n", firstNames)

			if remainingTickets == 0 {
				// end program
				fmt.Println("Our Conference is booked out. Come back next year.")
				break
			}

		} else {
			if !isValidName {
				fmt.Println("First name or last name you entered is too short")
			}
			if !isValidEmail {
				fmt.Println("Email address you entered is doesn't contain @ sign")
			}
			if !isValidTicketNumber {
				fmt.Println("number of tickets you entered is invalid")
			}
		}

	}

	city := "London"

	switch city {
	case "New York":
		//case 1
	case "Singapore", "australia":
		//case 2
	case "London":
		//case 3
	case "Berlin":
		//case 4
	case "Mexico City":
		//case 5
	case "Hong Kong":
		//case 6
	default:
		fmt.Print("No Valid City Selected")
	}

}
