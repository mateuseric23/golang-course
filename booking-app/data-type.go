package main

import "fmt"

func main() {

	// fmt.Println("Welcome to our conference booking application")
	// fmt.Println("Get your tickets here to attend")

	// var conferenceName = "Go Conference"
	// _ = conferenceName
	// fmt.Println(conferenceName)

	conferenceName := "Go Conference"
	const conferenceTickets = 50
	var remainingTickets uint = 50

	fmt.Printf("Conference Tickets is %T, Remaining Tickets is %T, Conference Name is %T\n", conferenceTickets)

	fmt.Printf("Welcome to %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are still available.\n", conferenceTickets, remainingTickets)
	fmt.Println("Get your tickets here to attend")

	var userName string
	var userTickets int

	//ask user to their name

	userName = "Tom"
	userTickets = 2

	fmt.Printf("User %v booked %v tickets.\n", userName, userTickets)

}
